package latencyBot

import (
	"fmt"
	"image/color"
	"io"
	"sort"
	"time"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgimg"

	"github.com/prometheus/common/model"
)

var (
	latDark  = color.RGBA{24, 35, 47, 0xff}
	latLight = color.RGBA{232, 236, 255, 0xff}
)

type renderer struct {
	*plot.Plot
}

func NewRenderer(fontName string, width int) (*renderer, error) {
	plot, err := plot.New()
	if err != nil {
		return nil, err
	}

	plot.Title.Padding = 20
	//plot.X.Padding = 20
	//plot.Y.Padding = 20
	//	plot.Legend.XOffs = -20
	//	plot.Legend.YOffs = 50

	plot.Title.Color = latLight
	font, err := vg.MakeFont(fontName, 10)
	if err != nil {
		return nil, err
	}
	plot.Title.Font = font

	plot.Y.LineStyle.Color = latLight
	plot.Y.Label.Text = "Duration in Milliseconds"
	plot.Y.Label.Color = latLight
	plot.Y.Label.Font = font
	plot.Y.Tick.LineStyle.Color = latLight
	plot.Y.Tick.Label.Color = latLight

	plot.X.LineStyle.Color = latLight
	plot.X.Label.Color = latLight
	plot.X.Label.Font = font
	plot.X.Tick.LineStyle.Color = latLight
	plot.X.Tick.Label.Color = latLight

	plot.Legend.Top = true
	plot.Legend.Color = latLight
	plot.BackgroundColor = latDark
	grid := plotter.NewGrid()
	grid.Vertical.Width = 0
	plot.Add(grid)
	return &renderer{
		Plot: plot,
	}, nil
}

func RenderSamples(title, font string, samples map[string]model.Samples, w io.Writer) error {
	width := 500
	plot, err := NewRenderer(font, width)
	if err != nil {
		return err
	}
	plot.Title.Text = title

	probes := []string{}
	for probe, _ := range samples {
		probes = append(probes, probe)
	}
	sort.Strings(probes)

	phases := []string{
		"resolve",
		"connect",
		"tls",
		"processing",
		"transfer",
	}
	colors := []color.Color{
		color.RGBA{0x2F, 0x57, 0x5E, 0xFF},
		color.RGBA{0xEA, 0xB8, 0x39, 0xFF},
		color.RGBA{0x6D, 0x1F, 0x62, 0xFF},
		color.RGBA{0x62, 0x9E, 0x51, 0xFF},
		color.RGBA{0x64, 0xB0, 0xC8, 0xFF},
	}
	// plot.NominalY(phases...)

	// map of phases of targets
	results := map[model.LabelValue][]float64{}
	statusCodes := make([]float64, len(probes))
	successes := make([]float64, len(probes))

	sampleCount := len(samples) //FIXME: len(probes)??
	tlsExpiry := 0.0
	for i, probe := range probes {
		//		statusCode := "0"
		//		status := "FAIL"

		for _, sample := range samples[probe] {
			// one probe
			// results[phase][i]
			switch sample.Metric["__name__"] {
			case "probe_http_duration_seconds":
				phase := sample.Metric["phase"] // + sample.Metric["redirect"]
				if results[phase] == nil {
					// +1 for some empty space
					results[phase] = make([]float64, sampleCount+1)
				}
				results[phase][i] += float64(sample.Value) * 1000 // sum up, convert to ms
			case "probe_http_status_code":
				statusCodes[i] = float64(sample.Value)
			case "probe_success":
				successes[i] = float64(sample.Value)
			case "probe_ssl_earliest_cert_expiry":
				// Just use any
				tlsExpiry = float64(sample.Value)
			}
		}
	}

	plot.X.Label.Text = "Region"
	if tlsExpiry != 0.0 {
		exp := time.Unix(int64(tlsExpiry), 0)
		dur := exp.Sub(time.Now())
		plot.X.Label.Text = fmt.Sprintf("TLS Cert expires in %.0f days", (dur.Hours() / 24.0))
	}

	probeLabels := make([]string, len(probes))
	for i, probe := range probes {
		status := "FAIL"
		if successes[i] == 1.0 {
			status = "OK"
		}
		probeLabels[i] = fmt.Sprintf("%s %s: %.0f", probe, status, statusCodes[i])
	}
	plot.NominalX(probeLabels...)

	phaseM := make([][]float64, len(phases))
	for i, phase := range phases {
		phaseM[i] = results[model.LabelValue(phase)]
	}
	var pbar *plotter.BarChart
	for i, values := range phaseM {
		// We might not have timings for all phases
		if len(values) == 0 {
			continue
		}
		bar, err := plotter.NewBarChart(plotter.Values(values), vg.Length((width/sampleCount)/2))
		if err != nil {
			return err
		}
		bar.Color = colors[i]
		bar.LineStyle.Width = 0
		if pbar != nil {
			bar.StackOn(pbar)
		}
		pbar = bar
		plot.Add(bar)
		plot.Legend.Add(phases[i], bar)
	}

	c := vgimg.New(vg.Length(width), 300) //4*vg.Inch, 4*vg.Inch)
	cpng := vgimg.PngCanvas{Canvas: c}
	cv := draw.New(cpng)
	plot.Draw(cv)

	_, err = cpng.WriteTo(w)
	return err
}
