package main

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/ChimeraCoder/anaconda"
	"github.com/namsral/flag"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/latency.at/latencyBot"
	"gonum.org/v1/plot/vg"
	apiv1 "k8s.io/api/core/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/tools/clientcmd"
)

const defaultSearchTerm = "@latencyAt probe filter:links"

var (
	consumerKey     = flag.String("consumer-key", "", "Twitter Consumer Key")
	consumerSecret  = flag.String("consumer-secret", "", "Twitter Consumer Secret")
	accessKeyID     = flag.String("access-key", "", "Acccess Token")
	accessKeySecret = flag.String("access-secret", "", "Access Token Secret")
	searchTerm      = flag.String("search-term", defaultSearchTerm, "Search term")
	sinceID         = flag.Int("initial-since", 0, "ID of last tweet we responded to")

	latURLFlag = flag.String("url", "https://mon.latency.at/probe", "Latency.at Monitoring URL")
	latToken   = flag.String("token", "", "Latency.at Token")
	timeout    = flag.Duration("to", 10*time.Second, "Timeout for HTTP request to prober")

	fontName = flag.String("font", "NDKaputtOffice-Medium", "Font to use in rendered image")
	fontDir  = flag.String("font-dir", os.Getenv("HOME")+"/.fonts", "Path to font dir")

	metricsAddr   = flag.String("metrics-addr", ":8080", "Address to serve prometheus metrics on")
	kubeConfig    = flag.String("kube-config", "", "If set, use kubeconfig to connect to k8s")
	kubeConfigMap = flag.String("kube-configmap", "latency-bot", "Config map to store last tweet id in")
	kubeNamespace = flag.String("kube-namespace", "default", "Kubernetes Namespace to store configmap in")

	errTweetShort = errors.New("Tweet too short")

	errorCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "lat",
		Subsystem: "bot",
		Name:      "errors_total",
		Help:      "Total number of errors in latency.at bot",
	})
	tweetCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "lat",
		Subsystem: "bot",
		Name:      "tweets_total",
		Help:      "Total number of tweets responded to",
	})
	durationHistogram = prometheus.NewHistogram(prometheus.HistogramOpts{
		Namespace: "lat",
		Subsystem: "bot",
		Name:      "duration_seconds_total",
		Help:      "Duration histogram for processing tweets",
		Buckets:   []float64{2, 4, 8},
	})
)

func title(target, module string) string {
	return fmt.Sprintf("Latency.at %s results for %s", module, target)
}

func parseTweet(tweet *anaconda.Tweet) (target, module string, err error) {
	module = "http_2xx"
	parts := strings.Fields(tweet.Text)
	if len(parts) < 3 {
		return "", "", errTweetShort
	}
	if len(tweet.Entities.Urls) > 0 {
		target = tweet.Entities.Urls[0].Expanded_url
	}
	if len(parts) > 3 {
		for _, m := range []string{"http_2xx", "http_post_2xx"} {
			if parts[3] == m {
				module = m
				break
			}
		}
	}
	return target, module, nil
}

func processTweets(api *anaconda.TwitterApi, searchTerm string, latURL *url.URL, probes []string, since int64) (int64, error) {
	v := url.Values{}
	v.Add("result_type", "recent")
	if since != 0 {
		v.Add("since_id", strconv.FormatInt(since, 10))
	}

	log.Println("Searching...")
	results, err := api.GetSearch(searchTerm, v)
	if err != nil {
		return since, err
	}

	client := &http.Client{}
	for _, tweet := range results.Statuses {
		tweetCounter.Inc()
		start := time.Now()
		defer durationHistogram.Observe(time.Now().Sub(start).Seconds())
		log.Println("<", "@"+tweet.User.ScreenName, tweet.Id, tweet.Text)
		target, module, err := parseTweet(&tweet)
		if err != nil {
			log.Println(err)
			continue
		}
		values := url.Values{
			"target": []string{target},
			"module": []string{module},
		}
		log.Println(">", module, target)
		samples, err := latencyBot.ScrapeAll(client, *latToken, latURL, values, probes, *timeout)
		if err != nil {
			log.Println(err)
			continue
		}

		// resp := samplesToTweet(samples)

		var buf bytes.Buffer
		if err := latencyBot.RenderSamples(title(target, module), *fontName, samples, &buf); err != nil {
			log.Println(err)
			continue
		}
		//log.Println(buf.String())
		media, err := api.UploadMedia(base64.StdEncoding.EncodeToString(buf.Bytes()))
		if err != nil {
			log.Println(err)
			continue
		}
		log.Printf("Uploaded media: %#v", media)
		v := url.Values{}
		v.Set("media_ids", media.MediaIDString)
		v.Set("in_reply_to_status_id", tweet.IdStr)
		_, err = api.PostTweet("@"+tweet.User.ScreenName+" Latency.at "+module+" probe results for "+target, v)
		if err != nil {
			return since, err
		}
		if tweet.Id > since {
			since = tweet.Id
		}
	}
	return since, nil
}

func connectKube(kubeconfig string) (*kubernetes.Clientset, error) {
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		return nil, err
	}
	return kubernetes.NewForConfig(config)
}

func main() {
	var (
		probes = []string{"sfo1.do", "fra1.do", "nyc1.do", "sgp1.do", "blr1.do"}
	)
	flag.Parse()
	latURL, err := url.Parse(*latURLFlag)
	if err != nil {
		log.Fatal(err)
	}

	vg.FontDirs = append(vg.FontDirs, *fontDir)
	vg.FontMap[*fontName] = *fontName

	prometheus.MustRegister(errorCounter, tweetCounter, durationHistogram)
	http.Handle("/metrics", promhttp.Handler())
	go func() {
		log.Fatal(http.ListenAndServe(*metricsAddr, nil))
	}()

	anaconda.SetConsumerKey(*consumerKey)
	anaconda.SetConsumerSecret(*consumerSecret)
	api := anaconda.NewTwitterApi(*accessKeyID, *accessKeySecret)

	kclient, err := connectKube(*kubeConfig)
	if err != nil {
		log.Fatal(err)
	}

	since, err := kubeGetID(kclient)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Searching for", *searchTerm)
	for {
		id, err := processTweets(api, *searchTerm, latURL, probes, since)
		if err != nil {
			log.Println(err)
			errorCounter.Inc()
		} else {
			if id > since {
				since = id
				if err := kubeSetID(kclient, since); err != nil {
					log.Println(err)
					errorCounter.Inc()
					continue
				}
			}
			since = id
		}
		log.Println("Sleeping")
		time.Sleep(1 * time.Minute)
	}
}

func kubeGetID(client *kubernetes.Clientset) (int64, error) {
	cm, err := client.CoreV1().ConfigMaps(*kubeNamespace).Get(*kubeConfigMap, metav1.GetOptions{})
	if err != nil {
		if kerrors.IsNotFound(err) {
			// create
			_, err := client.CoreV1().ConfigMaps(*kubeNamespace).Create(kubeIDConfigMap(int64(*sinceID)))
			if err != nil {
				return 0, errors.New("Couldn't create configmap: " + err.Error())
			}
			return int64(*sinceID), nil
		}
		return 0, err
	}
	return strconv.ParseInt(cm.Data["last_tweet_id"], 10, 64)
}

func kubeSetID(client *kubernetes.Clientset, id int64) error {
	log.Println("Set last ID to", id)
	_, err := client.CoreV1().ConfigMaps(*kubeNamespace).Update(kubeIDConfigMap(id))
	return err
}

func kubeIDConfigMap(id int64) *apiv1.ConfigMap {
	return &apiv1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name: *kubeConfigMap,
		},
		Data: map[string]string{
			"last_tweet_id": strconv.FormatInt(id, 10),
		},
	}
}
