package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"gonum.org/v1/plot/vg"

	"github.com/namsral/flag"
	"gitlab.com/latency.at/latencyBot"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

var (
	latURLFlag = flag.String("url", "https://mon.latency.at/probe", "Latency.at Monitoring URL")
	latToken   = flag.String("token", "", "Latency.at Token")
	timeout    = flag.Duration("to", 10*time.Second, "Timeout for HTTP request to prober")
	module     = flag.String("module", "http_2xx", "Module to use")
	file       = flag.String("filename", "out.png", "Filename to write")

	fontName = flag.String("font", "NDKaputtOffice-Medium", "Font to use in rendered image")
	fontDir  = flag.String("font-dir", os.Getenv("HOME")+"/.fonts", "Path to font dir")

	probes = []string{"sfo1.do", "fra1.do", "nyc1.do", "sgp1.do", "blr1.do"}
)

func main() {
	flag.Parse()
	target := flag.Args()[0]
	latURL, err := url.Parse(*latURLFlag)
	if err != nil {
		log.Fatal(err)
	}

	vg.FontDirs = append(vg.FontDirs, *fontDir)
	vg.FontMap[*fontName] = *fontName

	values := url.Values{
		"target": []string{target},
		"module": []string{*module},
	}
	samples, err := latencyBot.ScrapeAll(&http.Client{}, *latToken, latURL, values, probes, *timeout)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("%v", samples)

	var buf bytes.Buffer
	title := fmt.Sprintf("Latency.at %s results for %s", *module, target)

	if err := latencyBot.RenderSamples(title, *fontName, samples, &buf); err != nil {
		log.Fatal(err)
	}
	if err := ioutil.WriteFile(*file, buf.Bytes(), 0644); err != nil {
		log.Fatal(err)
	}
}
